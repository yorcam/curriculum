<?php
    class portafolio
    {
        private $con;
        function __construct(){
            include './conexion.php';
            $this->con = new conexion();
        }
        public function getAll(){
            $data = array();
            $query = "SELECT * FROM portafolio";
            $resultado = $this->con->select($query);
            foreach ($resultado as $res) {
                array_push($data, array(
                    "id" => (int) $res["p_id"],
                    "nombre" => $res["p_nombre"],
                    "resumen" => $res["p_resumen"],
                    "descripcion" => $res["p_descripcion"],
                    "link" => $res["p_link"],
                    "portada" => $res["p_foto_portada"]
                ));
            }
            return json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);
        }
        public function getportafolio($id){
            //$data = array();
            $query = "SELECT * FROM portafolio WHERE p_id = ".$id;
            $resultado = $this->con->select($query);
            foreach ($resultado as $res) {
                $data = array(
                    "id" => (int) $res["p_id"],
                    "nombre" => $res["p_nombre"],
                    "resumen" => $res["p_resumen"],
                    "descripcion" => $res["p_descripcion"],
                    "link" => $res["p_link"],
                    "portada" => $res["p_foto_portada"],
                    "galeria" => $this->imagesToArray($res["p_galeria"])
                );
            }
            return json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);
        }
        private function imagesToArray($images){
            $data = array();
            $converted = explode(";", $images);
            $contador = 1;
            foreach ($converted as $con) {
                array_push($data, array(
                    "imgId" => $contador,
                    "img" => $con
                ));
                $contador++;
            }
            return $data;
        }
    }
?>