<?php
    class experiencia
    {
        private $con;
        function __construct(){
            include './conexion.php';
            $this->con = new conexion();
        }
        public function getAll(){
            $data = array();
            $query = 'SELECT * FROM experiencia';
            $resultado = $this->con->select($query);
            foreach ($resultado as $res) {
                $fecha_inicio = explode(' ', $res['e_fecha_inicio'])[0];
                $fecha_inicio_corta = $this->transformMonth(explode('-', $fecha_inicio)[1]).' '.explode('-', $fecha_inicio)[0];
                $fecha_fin = explode(' ', $res['e_fecha_fin'])[0];
                $fecha_fin_corta = $this->transformMonth(explode('-', $fecha_fin)[1]).' '.explode('-', $fecha_fin)[0];
                
                array_push($data, array(
                    'id' => (int) $res['e_id'],
                    'nombre' => $res['e_empresa'],
                    'cargo' => $res['e_cargo'],
                    'direccion' => $res['e_direccion'],
                    'telefono' => $res['e_telefono'],
                    'fecha_inicio_larga' => $res['e_fecha_inicio'],
                    'fecha_inicio_corta' => $fecha_inicio_corta,
                    'fecha_fin_larga' => $res['e_fecha_fin'],
                    'fecha_fin_corta' => $fecha_fin_corta,
                    'comentario' => $res['e_comentario']
                ));
            }
            return json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);
        }

        private function transformMonth($mes){
            $mes = (int) $mes;
            switch ($mes) {
                case 1:
                    return 'Enero';
                    break;
                case 2:
                    return 'Febrero';
                    break;
                case 3:
                    return 'Marzo';
                    break;
                case 4:
                    return 'Abril';
                    break;
                case 5:
                    return 'Mayo';
                    break;
                case 6:
                    return 'Junio';
                    break;
                case 7:
                    return 'Julio';
                    break;
                case 8:
                    return 'Agosto';
                    break;
                case 9:
                    return 'Septiembre';
                    break;
                case 10:
                    return 'Octubre';
                    break;
                case 11:
                    return 'Noviembre';
                    break;
                case 12:
                    return 'Diciembre';
                    break;
            }
        }
    }
    
?>