<?php
	/**
	* Clase que contiene la conexion a la base de datos
	*/
	class conexion
	{
		private $host = 'localhost';
		private $usr = 'root';
		private $pas = '';
		private $db = 'yorcamdb';
		public $link;
		public $conexion = 0;

		function __construct()
		{
			$this->link = mysqli_connect($this->host, $this->usr, $this->pas, $this->db);
			if (mysqli_connect_errno()) {
				echo "Fallo al conectar a MySQL: (".mysqli_connect_error().") ".mysqli_connect_error();
			}
			mysqli_set_charset($this->link, "utf8"); // acentos normales del español --> á Á ñ, entre otros
			$this->conexion = 1;
		}
		public function select($query){
			$res = mysqli_query($this->link, $query);
			return $res;
		}
	}
?>