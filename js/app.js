angular.module("myApp", ["lumx", "ngRoute", "ngPagination", "ngResource"])
.config(function($routeProvider){
    $routeProvider
        .when("/", {
            controller: "aboutMe",
            templateUrl: "templates/about-me.html"
        })
        .when("/portafolio/:id", {
            controller: "portafolio",
            templateUrl: "templates/portafolio.html"
        })
        .otherwise("/");
});