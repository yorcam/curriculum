angular.module("myApp")
.controller("aboutMe", function($scope, $resource){

    Experiencia = $resource("http://127.0.0.1/curriculum2/lib/php/getExperiencia.php", {id:"@id"});
    $scope.experiencia = Experiencia.query();

    Portafolio = $resource("http://127.0.0.1/curriculum2/lib/php/getPortafolio.php", {id:"@id"});
    $scope.portafolio = Portafolio.query();
})
.controller("portafolio", function($scope, $resource, $routeParams){
    Portafolio = $resource("http://127.0.0.1/curriculum2/lib/php/getPortafolio.php", {id:"@id"});
    $scope.portafolio = Portafolio.get({id: $routeParams.id});
    console.log($scope.portafolio);
});